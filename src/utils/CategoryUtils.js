const CategoryUtils = {

	formatCategories (categories , onlyShowParent) {
		let result = [];
        for (let i = 0; i < categories.length; i++) {
            let cat1 = {};
            cat1.value = categories[i].id;
            cat1.label = categories[i].name;
            cat1.children = [];
            let cats2 = categories[i].subCats;
            for (let j = 0; j < cats2.length; j++) {
                let cat2 = {};
                cat2.value = cats2[j].id;
                cat2.label = cats2[j].name;
                cat2.children = [];
                if(!onlyShowParent){
                	let cats3 = cats2[j].subCats;
                	for (let k = 0; k < cats3.length; k++) {
	                    let cat3 = {};
	                    cat3.value = cats3[k].id;
	                    cat3.label = cats3[k].name;
	                    cat3.children = [];
	                    cat2.children.push(cat3);
	                }
                }
                cat1.children.push(cat2);
            }
            result.push(cat1);
        }
        return result;
	},

	populateParents (categories , catId) {
		let result = [];
		if(!categories || ! catId){
			return result
		}
        for (let i = 0; i < categories.length; i++) {
            let p1 = categories[i].value;
            let cats2 = categories[i].children;
            for (let j = 0; j < cats2.length; j++) {
                let p2 = cats2[j].value;
                if(p2 == catId){
                	result.push(p1)
                	result.push(p2)
                	return result
                }
            	let cats3 = cats2[j].children;
            	for (let k = 0; k < cats3.length; k++) {
                    let p3 = cats3[k].value;
                    if(p3 == catId){
                    	result.push(p1)
	                	result.push(p2)
	                	result.push(p3)
	                	return result
                    }
                }
            }
        }
        return result;
	}
}

export default CategoryUtils;