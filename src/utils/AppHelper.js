
function init(){

	let helper = {
		mode : 'dev',
		config : {
			catalogServiceUrl : 'http://localhost:8801',
			serviceUrl : 'http://localhost:8081/bigfans-service/api'
		},
		api : {
			buildGetUrl : function(url , params){
				let s = [];
				for(let name in params){
					let value = params[name];
					if(typeof(value) !== "undefined"){
						s.push(encodeURIComponent(name) + "=" + encodeURIComponent(value))
					}
				}
				if(s.length !== 0){
					url += "?";
					url += s.join("&");
				}
				return url;
			}
		},

		Arrays : {
			pluck : function(list , key){
				let results = [];
				for(let i=0;i<list.length;i++){
					let obj = list[i];
					results.push(obj[key]);
				}
				return results;
			}
		},
		
		tools : {
			formatCategories : function(categories , onlyShowParent) {
				let result = [];
		        for (let i = 0; i < categories.length; i++) {
		            let cat1 = {};
		            cat1.value = categories[i].id;
		            cat1.label = categories[i].name;
		            cat1.children = [];
		            let cats2 = categories[i].subCats;
		            for (let j = 0; j < cats2.length; j++) {
		                let cat2 = {};
		                cat2.value = cats2[j].id;
		                cat2.label = cats2[j].name;
		                cat2.children = [];
		                if(!onlyShowParent){
		                	let cats3 = cats2[j].subCats;
		                	for (let k = 0; k < cats3.length; k++) {
			                    let cat3 = {};
			                    cat3.value = cats3[k].id;
			                    cat3.label = cats3[k].name;
			                    cat3.children = [];
			                    cat2.children.push(cat3);
			                }
		                }
		                cat1.children.push(cat2);
		            }
		            result.push(cat1);
		        }
		        return result;
			},
		}
	}


	return helper;
}

export default init();


