const StringUtils = {
	split(str , separator){
		if(!str || str.length == 0){
			return []
		}
		return str.split(separator)
	}
}

export default StringUtils