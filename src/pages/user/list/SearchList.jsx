import React from 'react';
import {Table , Breadcrumb ,Card} from 'antd'
import { Link } from 'react-router-dom';
import 'whatwg-fetch'


const columns = [
  { title: '商品名称', width: 100, dataIndex: 'name', key: 'name' ,fixed: 'left'},
  { title: '分类', width: 100, dataIndex: 'age', key: 'age'},
  { title: '货号', dataIndex: 'address', key: '1', width: 100 },
  { title: '规格', dataIndex: 'spec', key: 'spec', width: 100 },
  { title: '价格', dataIndex: 'address', key: '2', width: 100 },
  { title: '推荐', dataIndex: 'address', key: '3', width: 100 },
  { title: '新品', dataIndex: 'address', key: '4', width: 100 },
  { title: '热卖', dataIndex: 'address', key: '5', width: 100 },
  { title: '上/下架', dataIndex: 'address', key: '6', width: 100 },
  { title: '库存', dataIndex: 'address', key: '7', width: 100 },
  { title: '规格', dataIndex: 'address', key: '8' },
  {
    title: 'Action',
    key: 'operation',
    fixed: 'right',
    width: 100,
    render: (row,a,index) => {
    	return (
    		<Link to={`/app/product/${row.key}`}>编辑</Link>
    		)
    }
  },
];

class ProductListPage extends React.Component {

	constructor(props) {
		super(props);
	}

	componentWillMount(){
		if(this.props.hideAction){
			this.columns = columns.slice(0,columns.length-2);
		} else {
			this.columns = columns;
		}
	}

	render () {
		const rowSelection = {
			onChange: (selectedRowKeys, selectedRows) => {
				this.props.onSelectChange(selectedRowKeys, selectedRows);
			},
		};
		return (
				<Table columns={this.columns} 
					   size="middle" 
					   bordered
					   rowSelection={this.props.showCheckbox && rowSelection}
					   pagination={this.props.pagination}
					   dataSource={this.props.dataSource} 
					   scroll={{x: '150%'}}
					   loading={this.props.loading}
					   onChange={this.handleTableChange} />
			)
	}
}

export default ProductListPage;