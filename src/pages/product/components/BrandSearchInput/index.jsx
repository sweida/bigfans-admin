import React from 'react';
import AppHelper from 'utils/AppHelper';
import SearchInput from 'components/SearchInput/SearchInput'

class BrandSearchInput extends SearchInput {

	constructor(props){
		super(props);
		this.url = AppHelper.config.serviceUrl + '/brands/search';
	}

	getRequestUrl(keyword){
		if(!keyword){
			return;
		}
		let url = this.url;
		url += "?keyword=" + keyword;
		let catIds = this.props.form.getFieldValue('productGroup.categoryId');
		if(catIds && catIds.length !== 0){
			let categoryId = catIds[0];
			url += "&categoryId=" + categoryId;
		}
		
		return encodeURI(url);
	}

	onSelect = (value) => {
		this.setState({ value })
		this.props.form.setFieldsValue({'productGroup.brandId' : value});
	}

	handleResponse(response){
		const data = response.data.brands.map(brand => ({
              text: brand.name,
              value: brand.id,
              fetching: false,
            }));
		return data;
	}

}

export default BrandSearchInput;

