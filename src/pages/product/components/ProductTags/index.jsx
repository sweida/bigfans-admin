import React from 'react';
import { AutoComplete, Input } from 'antd';
import TagGroup from 'components/TagGroup/TagGroup'
const { TextArea } = Input;

class ProductTags extends TagGroup {
  
  onCreate(newValue){
  	let currentTags = this.props.form.getFieldValue('productGroup.tags') || [];
  	let tagObj = {value : newValue};
  	currentTags.push(tagObj);
  	this.props.form.setFieldsValue({'productGroup.tags' : currentTags});
  }
  
  onRemove(removedValue){
	let currentTags = this.props.form.getFieldValue('productGroup.tags') || [];
	currentTags.filter(tag => tag.value !== removedValue);
	this.props.form.setFieldsValue({'productGroup.tags' : currentTags});
  }
}

export default ProductTags;