import React from 'react';
import {Form, Input, Breadcrumb, Card, InputNumber, Select, Radio, DatePicker , Checkbox, Button, Col , Switch} from 'antd';
import BaseComponent from 'components/BaseComponent'
const { RangePicker } = DatePicker;
const Option = Select.Option;

class OrderPromotionCreate extends BaseComponent{

    state = {
        type : 'EFD'
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
        //let name = this.props.form.getFieldValue('name');
        //let origin = this.props.form.getFieldValue('origin');
        // 获取表单中数据
        let formVals = this.props.form.getFieldsValue();
        console.info(formVals);
    }

    handleTypeChange = (type) => {
        this.setState({type});
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>促销管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建订单促销</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <Form onSubmit={e => {this.handleSubmit(e)}}>
                        <Form.Item {...formItemLayout} label="活动名称" hasFeedback >
                            {getFieldDecorator('name', {
                                rules: [{required: true, message: '请填写活动名称!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="活动类型" hasFeedback >
                            {getFieldDecorator('type', {
                                initialValue: 'EFD',
                                rules: [{required: true, message: '请选择活动类型!'}],
                            })(
                                <Select onChange={this.handleTypeChange}>
                                    <Select.Option value="EFD">满额免运费</Select.Option>
                                    <Select.Option value="EG">满额送优惠券</Select.Option>
                                </Select>
                            )}
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="活动期限" hasFeedback>
                            <Col span={5}>
                            <Form.Item>
                                {getFieldDecorator('startTime',{
                                    rules: [{required: true}],
                                })(
                                    <DatePicker
                                      showTime
                                      format="YYYY-MM-DD HH:mm:ss"
                                      placeholder="开始日期"
                                    />
                                )}
                            </Form.Item>
                          </Col>
                          <Col span={2}>
                            <p className="ant-form-split">-</p>
                          </Col>
                          <Col span={4}>
                            <Form.Item>
                                {getFieldDecorator('endTime',{
                                    rules: [{required: true, message: ''}],
                                })(
                                    <DatePicker
                                      showTime
                                      format="YYYY-MM-DD HH:mm:ss"
                                      placeholder="结束日期"
                                    />
                                )}
                            </Form.Item>
                          </Col>
                        </Form.Item>
                        <Form.Item {...formItemLayout} label="满足额度" hasFeedback >
                            {getFieldDecorator('condition', {
                                rules: [{required: true, message: '请填写满足额度!'}],
                            })(
                                <InputNumber style={{width:'auto'}}/>
                            )}
                        </Form.Item>
                        {
                            this.state.type == 'ER'
                            &&
                            <Form.Item {...formItemLayout} label="减免额度" hasFeedback >
                                {getFieldDecorator('reduction', {
                                    rules: [{required: true, message: '请填减免额度!'}],
                                })(
                                    <InputNumber style={{width:'auto'}}/>
                                )}
                            </Form.Item>
                        }
                        {
                            this.state.type == 'ED'
                            &&
                            <Form.Item {...this.formItemLayout} label="折扣" hasFeedback >
                                {this.getFieldDecorator('discount', {
                                    rules: [{required: true, message: '请填写折扣!'}],
                                })(
                                    <InputNumber style={{width:'auto'}}
                                                 min={0}
                                                 max={100}
                                                 formatter={value => `${value}%`}
                                                 parser={value => value.replace('%', '')}/>
                                )}
                            </Form.Item>
                        }
                        <Form.Item {...this.tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" size="large">确认</Button>
                        </Form.Item>
                    </Form>
                </Card>
            </div>
        );
    }
}

const OrderPromotionCreatePage = Form.create()(OrderPromotionCreate);

export default OrderPromotionCreatePage;